import XCTest

import NWSObservationTests

var tests = [XCTestCaseEntry]()
tests += NWSObservationTests.allTests()
XCTMain(tests)
