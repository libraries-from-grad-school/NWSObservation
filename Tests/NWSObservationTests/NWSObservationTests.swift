import XCTest
@testable import NWSObservation

final class NWSObservationTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        do {
            let observation = try NWSObservation(stationID: "KCVO")
            
            print(observation.pressureMb!)

        } catch {
            XCTAssert(false)
        }
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
